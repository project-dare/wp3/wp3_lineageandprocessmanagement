package nl.knmi;

import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.*;

import java.time.ZonedDateTime;

public class UseGeneratedBeans {

    private static final String TEST_NAMESPACE = "http://testnamespace.org";


    public static void main(String[] args) {

        ProvFactory provFactory = InteropFramework.newXMLProvFactory();
        CreateSessionBindingsBean bindingsBean = new CreateSessionBindingsBean(provFactory);

        // Initialize User
        bindingsBean.addUser(provFactory.newQualifiedName(TEST_NAMESPACE, "user", "Prefix"));
        bindingsBean.addAuthmode("authenticationMode");
        bindingsBean.addGroup("group");
        bindingsBean.addName("testUser");

        // Initialize NotebookAPI
        bindingsBean.addNotebookapi(provFactory.newQualifiedName(TEST_NAMESPACE, "notebookApi", "Prefix"));
        bindingsBean.addNameApi("testAPI");

        // Initialize K8Srecipe
        bindingsBean.addK8srecipe(provFactory.newQualifiedName(TEST_NAMESPACE, "k8Recipe", "Prefix"));
        bindingsBean.addVersion("v0.1");
        bindingsBean.addRecipeloc("recipeLocation");

        // Initialize Libs
        bindingsBean.addLibs(provFactory.newQualifiedName(TEST_NAMESPACE, "libs", "Prefix"));

        // Initialize Lib
        bindingsBean.addLib(provFactory.newQualifiedName(TEST_NAMESPACE, "lib1", "Prefix"));
        bindingsBean.addInstallationmode(("installMode1"));
        bindingsBean.addLibname("lib1");
        bindingsBean.addLibversion("1");
        bindingsBean.addLibprovtype(provFactory.newQualifiedName(TEST_NAMESPACE, "userLib", "Prefix"));

        bindingsBean.addLib(provFactory.newQualifiedName(TEST_NAMESPACE, "lib2", "Prefix"));
        bindingsBean.addInstallationmode(("installMode2"));
        bindingsBean.addLibname("lib2");
        bindingsBean.addLibversion("2");
        bindingsBean.addLibprovtype(provFactory.newQualifiedName(TEST_NAMESPACE, "systemLib", "Prefix"));

        // Initialize SystemImage
        bindingsBean.addSystemimage(provFactory.newQualifiedName(TEST_NAMESPACE, "systemImage", "Prefix"));
        bindingsBean.addSystemimagereference("systemImageRef");

        // Initialize Volumes
        bindingsBean.addVolume(provFactory.newQualifiedName(TEST_NAMESPACE, "DataVolume", "Prefix"));
        bindingsBean.addVolumeid("UUID-1234-Data");
        bindingsBean.addVolume(provFactory.newQualifiedName(TEST_NAMESPACE, "WorkingVolume", "Prefix"));
        bindingsBean.addVolumeid("UUID-1234-Work");

        // Initialize Jupyter
        bindingsBean.addJupyter(provFactory.newQualifiedName(TEST_NAMESPACE, "jupyter", "Prefix"));
        bindingsBean.addSessionid("SessionID");
        bindingsBean.addAccessurl("accessUrl");
        bindingsBean.addGeneratedat("1970-01-01T01:01:00+01:00");

        // Initialize CreateJupyter
        bindingsBean.addCreatejupyter(provFactory.newQualifiedName(TEST_NAMESPACE, "CreateJupyterActivity", "Prefix"));
        bindingsBean.addMethodPath("Method_PATH");
        bindingsBean.addEndtime(ZonedDateTime.now().toOffsetDateTime().toString());
        bindingsBean.addStarttime(ZonedDateTime.now().toOffsetDateTime().toString());
        bindingsBean.addNotebookid("UUID-NB-1234");

        // Initialize libList
        bindingsBean.addLiblist(provFactory.newQualifiedName(TEST_NAMESPACE, "LibList", "Prefix"));
        bindingsBean.addLiblistvalue("LibListValue");
        bindingsBean.addVersion("LibListVersion");

        // Export the bindings to Json
        bindingsBean.getBindings().addVariableBindingsAsAttributeBindings();
        bindingsBean.getBindings().exportToJson("bindings" + ".json");
    }
}
