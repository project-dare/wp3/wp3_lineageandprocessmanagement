# Generate java bindings example
This project illustrates how to generate java bindings from a provn template on the fly using ProvToolbox.
It also uses the generated java bindings to export them as a json file.

The template used in this case is: `../templates/create_session.provn`

## Setup
The pom.xml is configured such that it will generate bindings for create_session.provn in the generate-sources phase.
These source are then added to the classpath of the main project.

In the main project there is one java file, UseGeneratedBeans.java, which uses the generated beans to generate a bindings
file called `bindings.json`

## Usage
To generate the java bindings and run a program which uses them call the following commands:
```bash
mvn package
java -cp target/generate-java-bindings-example-1.0-SNAPSHOT-jar-with-dependencies.jar nl.knmi.UseGeneratedBeans
```

The output is a file called `bindings.json`, which can be used to expand the `create_session.provn` template

To post the bindings to a template expansion service you can use test_expansion.py as inspiration.
Ensure that you fill in the correct hostname and template id.