import requests
import urllib
import sys

host_name = 'localhost'
template_id = sys.argv[1]

bindings = sys.argv[2]
with open(bindings, 'r') as bindingsfile:
    data = bindingsfile.read()

print("Expanding bindings from %s" % bindings)

# url_encoded_trig_string = urllib.urlencode({"bindings": data})
# r = requests.get('https://' + host_name + '/templates/' + template_id +
#                  '/expand?fmt=provn&writeprov=false&bindver=v3&' + url_encoded_trig_string,
#                  verify=False)

r = requests.post('https://' + host_name + '/templates/' + template_id +
                  '/expand?fmt=provn&writeprov=false&bindver=v3',
                  data=data, verify=False)

print(r)
print(r.text)
