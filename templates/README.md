This folder contains PROVN templates as well as example binding files which can be used to expand the template.

# Templates

## Create notebook 

Openprovenance link: https://openprovenance.org/store/documents/1998
Template catalog ID: 5e4297163b832a1247611a02

## Update notebook

Openprovenance link: https://openprovenance.org/store/documents/1997
Template catalog ID:5e4297b43b832a1247611a03

## Run workflow

Openprovenance link: https://openprovenance.org/store/documents/1996
Template catalog ID: 5e42980c3b832a1247611a04

# Notes:
1. It seems currently impossible to expand vargen variables with user specified values when using the v3
    json bindings format. This also does not work when expanding templates with the ProvToolBox.
1. It seems currently impossible to expand the bundle id with a variable via the ProvTemplateCatalog.
    This does work in when expanding the template with the ProvToolBox.
    As a workaround a vargen variable can be used as bundle id, although this again cannot be substituted with
    a user specified value, only with a generated value.
