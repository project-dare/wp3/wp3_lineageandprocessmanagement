AWSTemplateFormatVersion: 2010-09-09
Description: Provenance Template catalog service stack

Resources:

  ENVRIFairSecurityGroup:
      Type: AWS::EC2::SecurityGroup
      Properties:
        GroupName: envrifair-sg
        GroupDescription: 'Security Group for public ENVRIFair services'
        VpcId: vpc-0b2f589956c1e8376
        SecurityGroupIngress:
          - IpProtocol: "tcp"
            FromPort: 22
            ToPort: 22
            CidrIp: 145.23.254.101/32
            Description: "SSH from KNMI LAN+WLAN"
          - IpProtocol: "tcp"
            FromPort: 22
            ToPort: 22
            CidrIp: 145.23.31.254/32
            Description: "SSH from KNMI VPN (F5)"
          - IpProtocol: "tcp"
            FromPort: 443
            ToPort: 443
            CidrIp: 0.0.0.0/0
        Tags:
          - Key: "Project Name"
            Value: "ENVRIFair"
          - Key: "Contact Person Name"
            Value: "alessandro.spinuso@knmi.nl"
          - Key: "SAP Internal Order Number"
            Value: "443003420964"
          - Key: "InternalOrderNumber"
            Value: "00000100"
          - Key: "Creation Time"
            Value: "2020-01-28"
          - Key: "Technical contact"
            Value: "eu-team@knmi.nl"

  ## Role is given access to pull from the ECR registry, even though at this point
  ## we do not use it. However, it might be needed at some point to get a ready
  ## made image up and running quick. In that case the EC2 can pull the image.
  ENVRIFairECRRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: envrifair-ecr-role
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
              - ec2.amazonaws.com
            Action:
              - sts:AssumeRole
      Policies:
        - PolicyName: envrifair-s3-ro-policy
          PolicyDocument:
            Statement:
              - Effect: Allow
                Action:
                  - s3:ListBucket
                  - s3:GetObject
                  - s3:GetObjectVersion
                  - s3:PutObject
                Resource:
                  - Fn::ImportValue: ENVRIFairAssetsBucketArn
                  - Fn::Join:
                    - ''
                    - - Fn::ImportValue: ENVRIFairAssetsBucketArn
                      - "/*"
        - PolicyName: envrifair-secretsmgr-ro-policy
          PolicyDocument:
            Statement:
              - Effect: Allow
                Action:
                  - secretsmanager:GetResourcePolicy
                  - secretsmanager:GetSecretValue
                  - secretsmanager:DescribeSecret
                  - secretsmanager:ListSecretVersionIds
                Resource: !Sub arn:aws:secretsmanager:${AWS::Region}:${AWS::AccountId}:secret:envrifair-template-catalog-openid-pCuZvt
      Tags:
          - Key: "Project Name"
            Value: "ENVRIFair"
          - Key: "Contact Person Name"
            Value: "alessandro.spinuso@knmi.nl"
          - Key: "SAP Internal Order Number"
            Value: "443003420964"
          - Key: "InternalOrderNumber"
            Value: "00000100"
          - Key: "Creation Time"
            Value: "2020-01-28"
          - Key: "Technical contact"
            Value: "eu-team@knmi.nl"

  ENVRIFairInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
        - !Ref ENVRIFairECRRole

  ENVRIFairProvTemplateCatalogInstance:
    Type: "AWS::EC2::Instance"
    Properties:
      ImageId: ami-071f4ce599deff521
      InstanceType: t3.micro
      IamInstanceProfile: !Ref ENVRIFairInstanceProfile
      KeyName: 'DEV_KNMI_ENVRIFair'
      Monitoring: true
      SecurityGroupIds:
        - !Ref ENVRIFairSecurityGroup
      SubnetId: subnet-0263234130252496c
      Tags:
        - Key: "Project Name"
          Value: "ENVRIFair"
        - Key: "Contact Person Name"
          Value: "alessandro.spinuso@knmi.nl"
        - Key: "SAP Internal Order Number"
          Value: "443003420964"
        - Key: "InternalOrderNumber"
          Value: "00000100"
        - Key: "Creation Time"
          Value: "2020-01-28"
        - Key: "Technical contact"
          Value: "eu-team@knmi.nl"
        - Key: "Name"
          Value: "envri-prov-template-catalog"
      UserData:
        Fn::Base64:
          Fn::Sub:
          - |
            #!/bin/bash
            yum-config-manager --enable epel
            yum update -y && yum install -y aws docker git jq pwgen
            mkdir ~/bin
            curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
            chmod 755 /usr/bin/docker-compose
            service docker start
            mkdir -p -m 777 /data/prov
            usermod -a -G docker ec2-user
            cd ~ec2-user
            aws s3 cp s3://${ENVRIFairAssetsBucket}/TemplateData.dump ./TemplateData.dump
            aws secretsmanager get-secret-value --secret-id  envrifair-template-catalog-openid  --region eu-west-1 | jq -r '.SecretString | fromjson | to_entries | map("export \(.key)=\(.value|tostring)")|.[]' > oauth-keys.txt
            git clone https://github.com/neutvd/ProvTemplateCatalog.git
            cd ProvTemplateCatalog
            dnsname=$(curl http://169.254.169.254/latest/meta-data/public-hostname)
            bash run-container.sh -d /data/prov -a ../oauth-keys.txt -h $dnsname -b $dnsname
            if [ -f ~ec2-user/TemplateData.dump ] ; then
              while [ docker-compose ps | grep mongo-db | grep Up -ne 0 ] ; do sleep 10 ; done
              docker cp ~ec2-user/TemplateData.dump mongo-db:/tmp
              docker exec mongo-db sh -c 'exec mongorestore --archive=/tmp/TemplateData.dump'
              echo '#!/bin/bash' > ~ec2-user/backup.sh
              echo "docker exec mongo-db sh -c 'exec mongodump -d TemplateData --archive' > /tmp/TemplateData.dump" >> ~ec2-user/backup.sh
              echo "aws s3 cp /tmp/TemplateData.dump s3://${ENVRIFairAssetsBucket}/TemplateData.dump" >> ~ec2-user/backup.sh
              echo "rm /tmp/TemplateData.dump" >> ~ec2-user/backup.sh
              chmod 755 ~ec2-user/backup.sh
              echo '0 0 * * * /home/ec2-user/backup.sh' | crontab
            fi
          - ENVRIFairAssetsBucket:
              Fn::ImportValue: ENVRIFairAssetsBucketName

  ENVRIFairTemplateCatalogAlarmTopic:
    Type: AWS::SNS::Topic
    Properties:
      Subscription:
        -
          Endpoint: "eu-team@knmi.nl"
          Protocol: "email"
      TopicName: "envrifair-template-catalog-alarms"
      Tags:
        - Key: "Project Name"
          Value: "ENVRIFair"
        - Key: "Contact Person Name"
          Value: "alessandro.spinuso@knmi.nl"
        - Key: "SAP Internal Order Number"
          Value: "443003420964"
        - Key: "InternalOrderNumber"
          Value: "00000100"
        - Key: "Creation Time"
          Value: "2020-01-28"
        - Key: "Technical contact"
          Value: "eu-team@knmi.nl"
        - Key: "Name"
          Value: "envri-prov-template-catalog"

  ENVRIFairTemplateCatalogAlarmTopicPolicy:
    Type: AWS::SNS::TopicPolicy
    Properties:
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              AWS: "*"
            Action: sns:Publish
            Resource: !Ref ENVRIFairTemplateCatalogAlarmTopic
            Condition:
              ArnLike:
                aws:SourceArn: arn:aws:cloudwatch:${AWS::Region}:${AWS::AccountId}:*
      Topics:
        - !Ref ENVRIFairTemplateCatalogAlarmTopic

  ENVRIFairTemplateCatalogCPUAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmDescription: CPU alarm for EC2 instance hosting prov template catalog.
      AlarmActions:
        - Ref: ENVRIFairTemplateCatalogAlarmTopic
      MetricName: CPUUtilization
      Namespace: AWS/EC2
      Statistic: Average
      Period: '60'
      EvaluationPeriods: '3'
      Threshold: '50'
      ComparisonOperator: GreaterThanThreshold
      Dimensions:
        - Name: InstanceId
          Value: !Ref 'ENVRIFairProvTemplateCatalogInstance'
