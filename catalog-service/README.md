# Deploying to AWS

You can deploy an new AWS EC2 instance that will build and start the docker container at
initialization. Also an alarm on CPU usage will be created. In case the CPU usage goes above
the treshold specified in the cloudformation file an email will be sent to eu-team@knmi.nl
or the address specified in the cloudformation file.

Run the following command to build the server. After a few minutes it should be up and running:

```
./deploy.sh
```

This will upload the mongo database archive from this repository and initialize the database
on the server with this data. It's possible you will get an error on the creation of the bucket
and the ECR registry because they already exist. This is fine and can be ignored.

To build and push the template catalog image to the RDWD research account read the instructions
in the [Prov Template Catalog readme](https://github.com/neutvd/ProvTemplateCatalog) to build
the image using the `bash ./run-container.sh` script in that repository. It will also start the
container, on your local machine, but you can kill it with `docker-compose down` from its
directory. An image with the tag `prov-template-svc:latest` will be created.

```
accountid=<your aws account id>
docker tag prov-template-svc:latest \
    ${accountid}.dkr.ecr.eu-west-1.amazonaws.com/envrifair/prov-template-svc:latest
$(aws ecr get-login --no-include-email --region eu-west-1)
docker push ${accountid}.dkr.ecr.eu-west-1.amazonaws.com/envrifair/prov-template-svc:latest
```

The image is also stored in the registry of [this repository at gitlab](https://gitlab.com/project-dare/wp3/wp3_lineageandprocessmanagement/container_registry).

```
docker tag prov-template-svc:latest registry.gitlab.com/project-dare/wp3/wp3_lineageandprocessmanagement/prov-template-svc:latest
docker push registry.gitlab.com/project-dare/wp3/wp3_lineageandprocessmanagement/prov-template-svc:latest
```

# Deploying to minikube for development

Determine the ingress end point of your minikube installation. On my machine it is
192.168.99.100. You can determine it by doing:
```
kubectl get -A ingress
```
Then clone the provenance template catalog repository at https://github.com/neutvd/ProvTemplateCatalog.git and build the docker container. Do not deploy to your minikube
cluster just yet. If you've already built it in the past and you still have the image
on your machine (`docker images | grep template`), you can skip this step. Alternatively
you can also pull the image from the gitlab container registry or the KNMI RDWD AWS
Research account ECR.
```
cd /tmp
git clone https://github.com/neutvd/ProvTemplateCatalog.git
bash ./run-container.sh -a ../oauth-keys.txt -h 192.168.99.100 -b 192.168.99.100
docker-compose down
```
This builds the docker image and also starts it, but we kill it immediately with the last command. Now we need to add the image to minikube's cache, because it will not be able to
pull it from your host machine, and also not from the KNMI-RDWD AWS Research account,
because it requires 2FA. We will first tag the image to the same name it has in the
AWS ECR and the kubernetes deployment specification YAML file in the ProvTemplateCatalog
repository and then we add it to minikube's cache:
```
docker tag prov-template-svc:latest ${accountid}.dkr.ecr.eu-west-1.amazonaws.com/envrifair/prov-template-svc:latest
minikube cache add ${accountid}.dkr.ecr.eu-west-1.amazonaws.com/envrifair/prov-template-svc:latest
## We also pull and add the mongodb image to minikube:
docker pull mongo:3.4.9
minikube cache add mongo:3.4.9
```
We need to make sure that `imagePullPolicy: Never` is specified in the deployment specs
`kubernetes.yaml` and `mongodb.yaml` and then run the following command to deploy to
kubernetes:
```
patch -p1< kubernetes/minikube.patch ## set imagePullPolicy to "never"
bash ./run-container.sh -a ../oauth-keys.txt -h 192.168.99.100 -b 192.168.99.100 -k
patch -p1 -R < kubernetes/minikube.patch ## Revert patch and remove imagePullPolicy field.
```

Now we can restore the database content in this repository into the running mongodb
container:
```
cd catalog-services
kubectl -n swirrl cp TemplateData.dump \
    `kubectl -n swirrl get pod | grep prov-template-catalog-mongodb | cut -d ' ' -f 1`:/tmp/TemplateData.dump
kubectl -n swirrl exec \
    `kubectl -n swirrl get pod | grep prov-template-catalog-mongodb  | cut -d ' ' -f 1` \
    -- mongorestore --archive=/tmp/TemplateData.dump
```

Now if we point our browser to the ingress end point we should see the provenance template catalog.
Follow the instructions on the [https://github.com/neutvd/ProvTemplateCatalog.git](ProvTemplateCatalog github page) linked above to get OpenID authentication
working. This is only needed if you want to add more templates through the gui in the
browser.