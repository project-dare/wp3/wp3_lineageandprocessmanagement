#!/bin/bash

echo
echo -n "Creating Elastic Container Registry."
aws cloudformation deploy --stack-name envri-prov-template-catalog-ecr-eu-west-1 \
    --capabilities CAPABILITY_NAMED_IAM --template-file ecr.cf.yaml
aws ecr put-image-scanning-configuration --repository-name envrifair/prov-template-svc \
    --image-scanning-configuration scanOnPush=true --region eu-west-1
echo
echo -n "Creating assets S3 bucket."
aws cloudformation deploy --stack-name envri-prov-template-catalog-bucket-eu-west-1 \
    --capabilities CAPABILITY_NAMED_IAM --template-file bucket.cf.yaml
BUCKETNAME=$(aws cloudformation describe-stacks --stack-name envri-prov-template-catalog-bucket-eu-west-1 \
            --query 'Stacks[0].Outputs[?OutputKey==`ENVRIFairAssetsBucketName`].OutputValue' --output text)
echo
echo -n "Uploading assets to assets bucket."
aws s3 cp ./TemplateData.dump s3://${BUCKETNAME}/TemplateData.dump
echo
echo -n "Deploying the provenance template catalog service."
aws cloudformation deploy --stack-name envri-prov-template-catalog-svc-eu-west-1 \
    --capabilities CAPABILITY_NAMED_IAM --template-file catalog-service.cf.yaml
